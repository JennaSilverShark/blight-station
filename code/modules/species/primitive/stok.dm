/datum/species/monkey/unathi
	name = "Stok"
	name_plural = "Stok"
	health_hud_intensity = 1.5

	icobase = 'icons/mob/human_races/species/monkey/stok_body.dmi'
	deform  = 'icons/mob/human_races/species/monkey/stok_body.dmi'

	tail = "stoktail"
	reagent_tag  = IS_UNATHI
	greater_form = SPECIES_UNATHI
	flesh_color  = "#34af10"
	base_color   = "#066000"
	force_cultural_info = list(
		TAG_CULTURE   = CULTURE_STOK,
		TAG_HOMEWORLD = HOME_SYSTEM_STATELESS,
		TAG_FACTION   = FACTION_TEST_SUBJECTS
	)