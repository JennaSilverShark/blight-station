/datum/species/monkey/alien
	name = "Farwa"
	name_plural = "Farwa"
	health_hud_intensity = 2

	icobase = 'icons/mob/human_races/species/monkey/farwa_body.dmi'
	deform  = 'icons/mob/human_races/species/monkey/farwa_body.dmi'

	tail = "farwatail"
	flesh_color = "#afa59e"
	base_color  = "#333333"
	force_cultural_info = list(
		TAG_CULTURE   = CULTURE_FARWA,
		TAG_HOMEWORLD = HOME_SYSTEM_STATELESS,
		TAG_FACTION   = FACTION_TEST_SUBJECTS
	)