/datum/species/monkey/skrell
	name = "Neaera"
	name_plural = "Neaera"
	health_hud_intensity = 1.75

	icobase = 'icons/mob/human_races/species/monkey/neaera_body.dmi'
	deform  = 'icons/mob/human_races/species/monkey/neaera_body.dmi'

	tail		 = null
	reagent_tag  = IS_SKRELL
	greater_form = SPECIES_SKRELL
	flesh_color  = "#8cd7a3"
	blood_color  = "#1d2cbf"
	force_cultural_info = list(
		TAG_CULTURE   = CULTURE_NEARA,
		TAG_HOMEWORLD = HOME_SYSTEM_STATELESS,
		TAG_FACTION   = FACTION_TEST_SUBJECTS
	)